<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

'appele_depuis' => 'Fonction appel&eacute;e par',

'chaines_langues' => 'Fichiers de langue',
'code' => 'Code',
'code_de_la' => 'Code de la',
'commentaire' => 'Commentaire',
'cree_le' => 'cr&eacute;&eacute;e le ',

'documentation' => 'Documentation collaborative',
'doubleclic' => 'Double-cliquez pour &eacute;diter',

'historique_article' => 'Modifications de la documentation',
'historique_fonction' => 'Historique de la fonction',

'message_ajoutez_doc' => 'Faites-vous plaisir et double cliquez pour l\'&eacute;crire...',
'message_ajoutez_spip_net' => 'Ajoutez un lien',
'message_ajoutez_traduction' => 'Traduire dans une autre langue',

'no_comment' => 'Aucune',

'parametres' => 'Param&egrave;tres',

'sur_spip_net' => 'Sur SPIP.net',

'traductions' => 'Traductions'
);

?>
