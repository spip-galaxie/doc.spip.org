<?php

function chaines_langues($lang, $module='public') {
	include_spip('inc/traduire');
	charger_langue($lang, $module);

	$tableau = $GLOBALS['i18n_' . $module . '_' . $lang];
	ksort($tableau);

	return $tableau;
}

function tableau_langue($tableau, $module='public') {
	$html = '<table class="spip">
	<tr class="titrem">
		<th>'._T('module_raccourci').'</th>
		<th>'._T('module_texte_affiche').'</th>
	</tr>
	';

	$aff_nom_module= "";
	if ($module != "public" AND $module != "local")
		$aff_nom_module = "$module:";

	$i = 0;
	foreach ($tableau as $raccourci => $val) {
		$bgcolor = alterner(++$i, 'row_even','row_odd');
		$html .= '
	<tr class="'.$bgcolor.'">
		<td><b>&lt;:'.$aff_nom_module.$raccourci.':&gt;</b></td>
		<td>'.$val.'</td>
	</tr>';
	}

	$html .= '</table>';

	return $html;
}

function selecteur_module($module='public') {
	include_spip('inc/lang');
	$selecteur = '';
	$modules = array();
	$fichiers = preg_files(repertoire_lang().'[a-z_]+\.php[3]?$');
	foreach ($fichiers as $fichier) {
		if (preg_match(',/([a-z]+)_([a-z_]+)\.php[3]?$,', $fichier, $r))
			isset($modules[$r[1]])?($modules[$r[1]] ++):($modules[$r[1]]=1);
	}

	$modules = array_keys($modules);

	if (count($modules) > 1) {
		$selecteur = '<ul>';
		foreach ($modules as $nom_module) {
			if ($nom_module == $module)
				$selecteur .= "<li><b>$nom_module</b></li>";
			else
				$selecteur .= "<li><a href='" .
					parametre_url(self(), 'module',$nom_module) .
					"'>$nom_module</a></li>";
		}
		$selecteur .= '</ul>';
	}

	return $selecteur;
}

?>