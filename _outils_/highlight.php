#!/usr/bin/php -n
<?php

ini_set('log', '/dev/null');

/* script permettant de transformer les references a doc.spip.org en
   liens vers le site */

$txt='';
while( ($r= fread(STDIN, 10000)) != '' ) $txt.=$r;

$out= highlight_string ($txt, true);
$out= preg_replace(
	'!(//(\*| |&nbsp;)*)(http://doc.spip.org/@(\S*?))((\*| |&nbsp;)*<br)!',
	'$1<A name="$4" href="$3">$3</A>$5', $out);
echo $out;
?>
