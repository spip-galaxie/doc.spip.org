#! /bin/sh
#script de surveillance du fichier de backend doc.spip.org a lancer dans un cron
if [ ! -f motif.txt ]; then
echo "analyse impossible : fichier motif.txt non trouve"
exit 1;
fi 

if [ ! -f old.rss ]; then
touch old.rss
fi

#recup fichier
wget -q -O new.rss "http://doc.spip.org/spip.php?page=backend_modifs"

#analyse
diff  -U 0  old.rss new.rss|grep -e '^\+\{1\}[^+]' |grep -f motif.txt>dif.log
if [ $? -eq 0 ]; then 
	mail -s "alerte sur doc.spip.org" spip-zone@rezo.net < dif.log
	echo "alerte " && dif.log 
fi
mv -f new.rss old.rss
exit 0;
